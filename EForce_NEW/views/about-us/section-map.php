<?php
/**
 * @author: Marko Mikulic
 */

/* @var $this \App\controllers\AboutUsController */
$latitude = get_field( 'google_map_latitude');
$longitude = get_field('google_map_longitude');
if ( is_numeric($latitude) && is_numeric($longitude) ) {
	?>
    <div class="full-width">
        <div id="map"></div>
    </div>
    <script>
        function initMap() {
            var mapDiv = document.getElementById('map');
            if (!mapDiv)
                return;
            var map = new google.maps.Map(mapDiv, {
                center: {
                    lat: <?= $latitude ?>,
                    lng: <?= $longitude ?>
                },
                zoom: 14,
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
                scaleControl: false
            });

            var marker = new google.maps.Marker({
                position: {
                    lat: <?= $latitude ?>,
                    lng: <?= $longitude ?>
                },
                map: map
            });
        }
    </script>
    <script defer async
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAngX3DsFtFo53TpOaYvg9aJ9KosIXQSZs&callback=initMap"></script>
	<?php
}
?>