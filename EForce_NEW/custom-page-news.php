<?php
/**
 * Template name: News
 * Author: Ante Galić
 */
use App\controllers\NewsController;

$controller = new NewsController();
$controller->render();