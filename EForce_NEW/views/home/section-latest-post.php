<?php
/**
 * @author: Marko Mikulic
 */

use rnd\helpers\Html;
use rnd\helpers\StringHelper;

/* @var $this \App\controllers\HomeController */
$featured_post_background_image = get_field( 'featured_post_background_image' );
$featured_news_list             = get_field( 'featured_news_list' );

$find_out_more_str = Rnd::$app->language == 'en' ? 'Find out more' : 'Mehr erfahren';

if ( have_rows( 'featured_news_list' ) ) {
	?>
    <div class="full-width eforce-container-with-bg text-white text-center">
        <div class="swiper-container featuredPostsHomeSlider">
            <div class="swiper-wrapper">
				<?php
				while ( have_rows( 'featured_news_list' ) ) {
					the_row();
					$current_post   = get_sub_field( 'post' );
					$featured_image = get_field( 'featured_image', $current_post->ID )['url'];
					$post_title     = $current_post->post_title;
					$post_content   = StringHelper::truncateWords( $current_post->post_content, 45 );
					?>
                    <div class="swiper-slide"
                         style="background-image: linear-gradient(#00000080, #00000080), url(<?= $featured_image ?>);">
                        <h1 class="heading1"><?= $post_title ?></h1>
						<?= Rnd::$app->getFormatter()->asParagraphs( $post_content ) ?>
                        <a href="#" class="btn eforce-btn btn-primary"><?= $find_out_more_str ?></a>
                    </div>
					<?php
				}
				?>
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-white"></div>
            <div class="swiper-button-prev swiper-button-white"></div>
        </div>
    </div>
	<?php
}
?>