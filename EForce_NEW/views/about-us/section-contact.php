<?php
/**
 * @author: Marko Mikulic
 */

use rnd\helpers\Html;

/* @var $this \App\controllers\AboutUsController */

$contact_title = get_field('contact_title', $this->pageID);
$contact_form_shortcode = get_field( 'contact_form_shortcode', $this->pageID);
?>
<div class="full-width static general-padding about-us-container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php
				echo Html::tag( 'h2', $contact_title, [ 'class' => 'heading h2' ] );
				?>
			</div>
		</div>
		<div class="row">
            <div class="col-md-6">
                <?= do_shortcode( $contact_form_shortcode) ?>
            </div>
            <?php
            if (have_rows('contact_list', $this->pageID)) {
	            ?>
                <div class="col-md-6 about-us contact-form">
                    <?php
                    while (have_rows('contact_list', $this->pageID)) {
	                    the_row();

	                    $address = get_sub_field( 'address' );
	                    $email   = get_sub_field( 'email' );
	                    $phone   = get_sub_field( 'phone' );
                        ?>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <?= Html::tag('p', '<strong>'.$address.'</strong>') ?>
                            </div>
                            <div class="col-md-6">
                                <?php
                                echo Html::tag('p', Html::mailto( $email));
                                echo Html::tag('p', $phone, ['class' => 'phone']);
                                ?>
                            </div>
                        </div>
	                    <?php
                    }
                    ?>
                </div>
	            <?php
            }
            ?>
		</div>
	</div>
</div>
