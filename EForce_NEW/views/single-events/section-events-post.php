<?php
/**
 * Author: Ante Galić
 */

use rnd\helpers\Html;
use rnd\helpers\Inflector;

$post = get_post();

$title        = get_the_title();
$date         = get_the_date( 'M d, Y' );
$post         = get_post( get_the_ID() );
$imageThumb   = get_field( 'image_thumb' );
$startDate    = get_field( 'start_date' );
$endDate      = get_field( 'end_date' );
$startingHour = get_field( 'starting_hour' );
$latitude     = get_field( 'latitude' );
$longitude    = get_field( 'longitude' );
$location     = get_field( 'location' );


$events_content                 = apply_filters( 'the_content', $post->post_content );
$featured_events_single_post_bg = get_the_post_thumbnail_url();


// Site language
$lang = Rnd::$app->language;
// Translations
$datetime_events_translation      = get_field( 'datetime_events_translation_' . $lang, 'options' );
$date_events_translation          = get_field( 'date_events_translation_' . $lang, 'options' );
$location_events_translation      = get_field( 'location_events_translation_' . $lang, 'options' );
$categories_events_translation    = get_field( 'categories_events_translation_' . $lang, 'options' );
$categories_list                  = get_the_terms( get_the_ID(), 'events-categories' );
$no_categories_events_translation = get_field( 'no_categories_events_translation_' . $lang, 'options' );
$categories_list_arr              = [];

foreach ( $categories_list as $item ) {
	array_push( $categories_list_arr, $item->name );
}

if ( empty( $categories_list_arr ) ) {
	$categories_list_arr = [ $no_categories_events_translation ];
}
?>
<div class="full-width static eforce-container-with-bg general-padding text-white text-center news-page-header"
     style="background-image: url(<?= $featured_events_single_post_bg ?>)">
    <div class="container">
        <div class="title-inner">
			<?= Html::tag( 'h2', $title, [ 'class' => 'fbold' ] ) ?>
        </div>
    </div>
</div>

<div class="container mtmb-70">
    <div class="row">
        <div class="col-md-6">
            <div class="col-sm-2">
				<?= Html::img( $featured_events_single_post_bg, [ 'width' => 52, 'height' => 69 ] ) ?>
            </div>
            <div class="col-sm-10 dateTime">
				<?= Html::tag( 'p', $datetime_events_translation ) ?>
                <div class="titleContentEvents">
					<?php
					echo Html::tag( 'p', "$date_events_translation - $startDate - $endDate" );
					echo Html::tag( 'i', $startingHour );
					?>
                </div>
            </div>
            <?php
            if ($location) {
                ?>
                <div class="col-sm-12 dateTime mt-5">
		            <?php
		            echo Html::tag( 'p', $location_events_translation );
		            echo Html::a( $location, '#' );
		            ?>
                </div>
            <?php
            }
            ?>
            <div class="col-sm-12 dateTime mt-5">
				<?php
				$categories      = Inflector::sentence( $categories_list_arr, ", " );
				$categories_span = Html::tag( 'span', $categories );
				echo Html::tag( 'p', $categories_events_translation . ' ' . $categories_span );
				?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="full-width">
				<?php
				if ( is_numeric( $latitude ) && is_numeric( $longitude ) ) {
				?>
                <div id="map"></div>
            </div>
            <script>
                function initMap() {
                    var mapDiv = document.getElementById('map');
                    if (!mapDiv)
                        return;
                    var map = new google.maps.Map(mapDiv, {
                        center: {
                            lat: <?=$latitude?>,
                            lng: <?=$longitude;?>
                        },
                        zoom: 14,
                        scrollwheel: false,
                        navigationControl: false,
                        mapTypeControl: false,
                        scaleControl: false
                    });

                    var marker = new google.maps.Marker({
                        position: {
                            lat: <?=$latitude;?>,
                            lng: <?=$longitude?>
                        },
                        map: map
                    });
                }
            </script>
			<?php
			}
			?>
            <script defer async
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAngX3DsFtFo53TpOaYvg9aJ9KosIXQSZs&callback=initMap"></script>
        </div>
        <div class="col-sm-12 eventsContent">
			<?= $events_content ?>
        </div>
    </div>
</div>
