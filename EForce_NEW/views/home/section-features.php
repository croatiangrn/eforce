<?php
/**
 * @author: Marko Mikulic
 */
use rnd\helpers\Html;

/* @var $this \App\controllers\HomeController */

$features_title = get_field('features_title', $this->pageID);
?>
<div class="full-width eforce-green-container general-padding">
	<div class="container">
		<?= Html::tag('h2', $features_title, ['class' => 'heading h2']) ?>
		<div class="row features">
            <?php
            $features_list_counter = 1;
            while (have_rows('features_list')) {
                the_row();
	            $image       = get_sub_field( 'image' );
	            $title       = get_sub_field( 'title' );
	            $description = get_sub_field( 'description' );

	            $divClass =  ['class' => 'feature-wrap'];

	            ?>
                <div class="col-sm-6">
                    <?php
                    echo Html::beginTag('div', $divClass);

                    echo Html::img($image['url'], ['alt' => $image['alt']]);
                    echo Html::tag('h3', $title, ['class' => 'heading h3']);
                    echo Html::tag('p', $description, ['class' => 'paragraph']);

                    echo Html::endTag('div');
                    ?>
                </div>
                <?php
	            $features_list_counter++;
            }
            ?>
		</div>
        <?php
        $features_button_text = get_field('features_button_text', $this->pageID);
        $features_button_link = get_field('features_button_link', $this->pageID);

        echo Html::a($features_button_text, $features_button_link, [
            'class' => 'btn eforce-btn uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-slide-bottom'
        ]);
        ?>
    </div>
</div>