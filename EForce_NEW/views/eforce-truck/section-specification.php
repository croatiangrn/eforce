<?php
/**
 * Author: Ante Galić
 */

use rnd\helpers\Html;

/* @var $this \App\controllers\EforceTruckController */
//$image = get_field( 'specification_image', $this->pageID )['url'];
$title = get_field( 'specification_title', $this->pageID );
$text  = get_field( 'specification_text', $this->pageID );
?>

<!--<div class="full-width eforce-green-container general-padding" id="<?/*= $title; */?>">
    <div class="optioncontent">
        <div class="container">
			<?php
/*			if ( $image !== null ) {
				*/?>
                <div><?/*= Html::img( $image ) */?></div>
				<?php
/*			}
			*/?>
            <h2 class="heading h2 secheading"><?/*= $title; */?></h2>
            <div class="row">
                <div class="col-md-12">
					<?/*= $text; */?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
/*if (have_rows('specification_content')) {
	*/?>
    <div class="optionpracticality">
        <div class="container">
            <div class="specificationpart">
                <div class="row">
					<?php
/*					$counter = 1;
					while ( have_rows( 'specification_content' ) ) {
						the_row();
						$specificationTitle = get_sub_field( 'specification_title' );
						$specificationText  = get_sub_field( 'specification_text' );
						$divRow             = ( $counter % 3 == 0 ) ? [ 'class' => 'row specificationpart' ] : [ 'class' => '' ];
						echo Html::beginTag( 'div', $divRow );
						echo Html::beginTag( 'div', [ 'class' => 'col-sm-4 uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-slide-left" data-uk-scrollspy="{cls:\'uk-animation-slide-left\', repeat:false, delay:300}' ] );
						echo Html::beginTag( 'div' );
						echo Html::tag( 'h4', $specificationTitle, [ 'class' => 'small-heading' ] );
						echo Html::tag( 'p', $specificationText, [ 'class' => '' ] );
						echo Html::endTag( 'div' );
						echo Html::endTag( 'div' );
						echo Html::endTag( 'div' );
						$counter ++;
					}

					*/?>
                </div>
            </div>
        </div>
    </div>
	--><?php
/*}
*/?>