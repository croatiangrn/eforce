<?php
/**
 * Author: Ante Galić
 */

namespace App\controllers;


use rnd\web\Controller;

class SingleNewsController extends Controller
{
    protected $viewName = 'single-news';
    protected $sections = [
        'news-post',
    ];

}