<?php
/**
 * @author: Marko Mikulic
 */
use rnd\helpers\Html;

/* @var $this \App\controllers\AboutUsController */
$careers_title = get_field('careers_title', $this->pageID);

if (have_rows('careers')) {
	?>
	<div class="full-width static general-padding about-us-container">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php
					echo Html::tag( 'h2', $careers_title, [ 'class' => 'heading h2' ] );
					?>
				</div>
			</div>
			<div class="row">
				<?php
				while ( have_rows( 'careers', $this->pageID ) ) {
					the_row();
					$career_title = get_sub_field( 'title' );
					$career_date  = get_sub_field( 'date' );

					$career_date_intl = Rnd::$app->getFormatter()->asDate($career_date, 'medium');
					echo Html::beginTag('div', ['class' => 'col-md-4 careers-wrap']);

					echo Html::tag('h3', $career_title, ['class' => 'text-gray']);
					echo Html::tag('p', $career_date_intl, ['class' => 'text-gray-lighter']);

					echo Html::endtag('div');
				}
				?>
			</div>
		</div>
	</div>
	<?php
}
?>