<?php
/**
 * Author: Ante Galić
 */

use rnd\helpers\Html;

/* @var $this \App\controllers\NewsController */
/* @var $latest_news WP_Query */
/* @var $posts_per_page int */

$latest_news_title = get_field( 'news_main_title' );
?>

<div class="full-width">
    <div class="latestNews">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
					<?= Html::tag( 'h4', $latest_news_title ) ?>
                </div>
            </div>
			<?php
			$latest_news_counter = 1;
			if ( $latest_news->have_posts() ) {
				while ( $latest_news->have_posts() ) {
					$latest_news->the_post();

					$title         = get_the_title();
					$url           = get_permalink();
					$content       = get_the_content( false );
					$date          = get_the_date( 'M, d Y' );
					$featuredImage = get_the_post_thumbnail_url() ?? null;

					if ( $latest_news_counter == 1 || $latest_news_counter % 2 == 1 ) {
						echo Html::beginTag( 'div', [ 'class' => 'row' ] );
					}
					?>
                    <div class="col-sm-6">
                        <a href="<?= $url ?>">
                            <?= Html::img($featuredImage, ['class' => 'img-responsive']) ?>
                            <h3>
                                <?= $title ?>
                            </h3>
                        </a>

                        <div class="dateNews">
							<?= $date ?>
                        </div>

                        <p>
							<?= $content ?>
                        </p>
                    </div>
					<?php
					if ( $latest_news_counter % 2 == 0 || $posts_per_page == $latest_news_counter || $latest_news->found_posts == $latest_news_counter ) {
						echo Html::endTag( 'div' );
					}
					$latest_news_counter ++;
				}
			}
			?>
        </div>
    </div>
</div>
