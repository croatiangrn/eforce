<?php
/**
 * @author: Marko Mikulic
 */

namespace App\controllers;


use rnd\web\Controller;
use WP_Query;

class DownloadsController extends Controller
{
	protected $sections = [
		'downloads'
	];

	protected function setPageTitle()
	{
		$this->headerParams['title'] = post_type_archive_title('', false) . ' - ' . get_bloginfo('name');
		$this->headerParams['title-wpn'] = post_type_archive_title('', false);
	}
}