<?php
/**
 * Author: Ante Galić
 */

use rnd\helpers\Html;

/* @var $this \App\controllers\EforceTruckController */
$image = get_field( 'economic_image', $this->pageID )['url'];
$title = get_field( 'economic_title', $this->pageID );
$text  = get_field( 'economic_text', $this->pageID );
?>
<div class="full-width eforce-green-container general-padding" id="<?= $title ?>">
    <div class="optioncontent">
        <div class="container">
	        <?php
	        if ($image !== null) {
		        ?>
                <div><?= Html::img($image) ?></div>
		        <?php
	        }
	        ?>
			<?= Html::tag( 'h2', $title, [ 'class' => 'heading h2' ] ) ?>
            <div class="row">
                <div class="col-md-12">
					<?= $text; ?>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="optionpracticality">
    <div class="container">
		<?php
		while ( have_rows( 'economic_content' ) ) {
			the_row();
			$economicText  = get_sub_field( 'economic_content_text' );
			$economicImage = get_sub_field( 'economic_content_image' )['url'];

			?>
            <div class="row practicalitypart">
                <div class="col-sm-12">
                    <?= $economicText ?>
                </div>
                <div class="col-sm-6 col-sm-offset-4">
                    <?= Html::img( $economicImage, [ 'class' => 'img-responsive' ] ) ?>
                </div>
            </div>
            <?php
		    }
		?>
    </div>
</div>

