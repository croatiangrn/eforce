<?php
/**
 * Author: Ante Galić
 */

use rnd\helpers\Html;

$current_post_bg = get_the_post_thumbnail_url();
$title           = get_the_title();
$date            = get_the_date( 'M d, Y' );
$post            = get_post( get_the_ID() );
?>
<div class="full-width static eforce-container-with-bg general-padding text-white text-center news-page-header"
     style="background-image: linear-gradient(#00000080, #00000080), url(<?= $current_post_bg ?>)">
    <div class="container">
        <div class="title-inner">
			<?= Html::tag( 'h2', $title, [ 'class' => 'fbold' ] ) ?>
        </div>
    </div>
</div>

<div class="singleNewsMiddle">
    <div class="container">
        <div class="newsAuthor">Author: <?php the_author_meta( 'user_nicename', $post->post_author ) ?></div>
		<?= Html::tag( 'div', $date, [ 'class' => 'newsDate' ] ) ?>
		<?= apply_filters( 'the_content', $post->post_content ) ?>
    </div>
</div>

<?php
/*$featuredNewsPostID = get_field( 'featured_news_post', 'options' );
if ( $featuredNewsPostID ) {
	$title = get_the_title( $featuredNewsPostID );
	$url   = get_the_permalink( $featuredNewsPostID );
	$post  = get_post( $featuredNewsPostID );
	*/?><!--
    <div class="full-width eforce-container-with-bg general-padding text-white text-center"
         style="background-image: linear-gradient(#00000080, #00000080), url(<?/*= get_the_post_thumbnail_url($featuredNewsPostID) */?>)">
        <div class="container">
            <div class="videoSecInner">
				<?php
/*				echo Html::tag( 'h3', $title, [ 'class' => 'fnews' ] );
				echo Html::tag( 'p', StringHelper::truncateWords( apply_filters( 'the_content', $post->post_content ), 25 ), [ 'class' => 'desnews' ] );
                $lang     = Rnd::$app->language;
                $mehr_str = $lang == 'en' ? 'Find out more' : 'Mehr erfahren';
                */?>
                <div class="text-center">
                    <?/*= Html::a( $mehr_str, $url, [ 'class' => 'btn eforce-btn1' ] ) */?>
                </div>
            </div>
        </div>
    </div>
	--><?php
/*}
*/?>
