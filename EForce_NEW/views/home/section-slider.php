<?php
/**
 * @author: Marko Mikulic
 */
use rnd\helpers\Html;

/* @var $this \App\controllers\HomeController */

$slider = get_field('slider_shortcode', $this->pageID );

echo Html::tag('div', do_shortcode($slider), ['class' => 'homepage-slider']);
