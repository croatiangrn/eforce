<?php
/**
 * @author: Marko Mikulic
 */
use rnd\helpers\Html;

/* @var $this \App\controllers\HomeController */
$video_background_image = get_field( 'video_background_image', $this->pageID );
$video_title            = get_field( 'video_title', $this->pageID );
$video_description      = get_field( 'video_description', $this->pageID );
$video_link             = get_field( 'video_link', $this->pageID );
$video_link_button_text = get_field( 'video_link_button_text', $this->pageID );
?>
<div class="full-width static eforce-container-with-bg general-padding text-white text-center" style="background-image: url(<?= $video_background_image['url'] ?>)">
	<div class="container">
		<div class="videoSecInner uk-scrollspy-init-inview uk-scrollspy-inview uk-animation-slide-bottom" data-uk-scrollspy="{cls:'uk-animation-slide-bottom', repeat:false, delay:300}">
			<?php
			echo Html::tag('h2', $video_title, ['class' => 'heading h2']);
			echo Html::tag('p', $video_description);

			echo Html::a($video_link_button_text, $video_link, ['class' => 'btn eforce-btn video-iframe']);
			?>
		</div>
	</div>
</div>
