<?php
/**
 * Author: Ante Galić
 */

use rnd\helpers\Html;

/* @var $this \App\controllers\EforceTruckController */

if ( have_rows( 'practicallity_list' ) ) {
	?>
    <div class="optionpracticality">
        <div class="container">
			<?php
			while ( have_rows( 'practicallity_list' ) ) {
				the_row();

				$title       = get_sub_field( 'title' ); // TEXT
				$description = get_sub_field( 'description' ); // WYSIWYG
				$image_1     = get_sub_field( 'image_1' ); // IMAGE OBJECT
				$image_2     = get_sub_field( 'image_2' ); // IMAGE OBJECT
				?>
                <div class="row practicalitypart">
                    <div class="col-sm-12">
                        <div class="practicalitytext">
							<?php
							echo Html::tag( 'h4', $title, [ 'class' => 'small-heading text-center' ] );
							echo $description;
							?>
                        </div>
                    </div>
					<?php
					if ( $image_1 ) {
					    ?>
                        <div class="imagecontent col-sm-6 col-sm-offset-4 text-center">
                            <?= Html::img( $image_1['url'], ['class' => 'img-responsive'] )?>
                        </div>
                        <?php
					}

					if ( $image_2 ) {
						?>
                        <div class="imagecontent col-sm-6 col-sm-offset-4 text-center">
							<?= Html::img( $image_2['url'], ['class' => 'img-responsive'] )?>
                        </div>
						<?php
					}
					?>
                </div>
				<?php
			}
			?>
        </div>
    </div>
	<?php
}
?>