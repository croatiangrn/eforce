<?php
/**
 * Template name: Single-News
 * Author: Ante Galić
 *
 *
 */

use App\controllers\SingleNewsController;

$controller = new SingleNewsController();
$controller->render();