<?php
/**
 * @author: Marko Mikulic
 */

namespace App\assets;


use rnd\base\AssetBundle;

class AppAsset extends AssetBundle
{
	public $css = [
		'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800',
		'css/uikit.min.css',
		'css/swiper.min.css',
		'css/magnific-popup.css',
		'css/main.css',
	];

	public $js = [
		'https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js',
		'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js',
		'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
		'js/uikit.min.js',
		'js/jquery.magnific-popup.min.js',
		'js/main.js',
	];
}