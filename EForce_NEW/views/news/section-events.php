<?php
/**
 * Author: Ante Galić
 */

use rnd\helpers\Html;
use rnd\helpers\StringHelper;

/* @var $this \App\controllers\NewsController */
/* @var $latest_events WP_Query */
?>

<div class="paddingNews">
    <div class="container">
        <?php
        while ($latest_events->have_posts()) {
	        $latest_events->the_post();

	        $title     = get_the_title();
	        $thumbnail = get_the_post_thumbnail_url();

	        $content = strip_tags(StringHelper::truncateWords( get_the_content(false), 25), 'p');
	        $url = get_the_permalink();
	        ?>
            <div class="row">
                <div class="singleNewsWrap">
                    <div class="col-md-1 br" style="padding: 0;">
                        <?= Html::img( $thumbnail, ['class' => 'img-responsive']) ?>
                    </div>
                    <div class="col-md-8 secondLabel pt-2">
                        <?php
                        echo Html::tag('h2', $title);
                        echo Html::tag('p', $content);
                        ?>
                    </div>
                    <div class="col-md-3 pt-2">
                        <?= Html::a('Mehr erfahren', $url, ['class' => 'btn btn-green mt-20 news-event-btn']) ?>
                    </div>
                </div>
            </div>
	        <?php
        }
        ?>
    </div>
</div>
