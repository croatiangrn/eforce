<?php
/**
 * @author: Marko Mikulic
 */
use rnd\helpers\Html;

/* @var $this \App\controllers\AboutUsController */

$about_us_title       = get_field( 'about_us_title', $this->pageID );
$about_us_description = get_field( 'about_us_description', $this->pageID );
?>
<div class="full-width static general-padding about-us-container text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php
				echo Html::tag('h2', $about_us_title, ['class' => 'heading h2']);
				echo $about_us_description;
				?>
			</div>
		</div>
	</div>
</div>
