<?php
/**
 * Author: Ante Galić
 */

use rnd\helpers\Html;

/* @var $this \App\controllers\EforceTruckController */
$image = get_field( 'technology_image', $this->pageID )['url'];
$title = get_field( 'technology_title', $this->pageID );
$text  = get_field( 'technology_text', $this->pageID );
?>

    <div class="full-width eforce-green-container general-padding" id="<?= $title; ?>">
        <div class="optioncontent">
            <div class="container">
                <div>
					<?= Html::img( $image ) ?>
                </div>
                <h2 class="heading h2"><?= $title; ?></h2>
                <div class="row">
                    <div class="col-md-12">
						<?= $text; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
if ( have_rows( 'technology_content' ) ) {
	?>
    <div class="optionpracticality">
        <div class="container">
			<?php
			while ( have_rows( 'technology_content' ) ) {
				the_row();
				$technologyText  = get_sub_field( 'technology_text' );
				$technologyImage = get_sub_field( 'technology_image' )['url'];
				echo Html::beginTag( 'div', [ 'class' => 'row practicalitypart' ] );
				echo Html::tag( 'div', $technologyText, [ 'class' => 'col-sm-12' ] );
				echo Html::beginTag( 'div', [ 'class' => 'col-sm-6 col-sm-offset-4' ] );
				echo Html::img( $technologyImage, [ 'class' => 'img-responsive' ] );
				echo Html::endTag( 'div' );
				echo Html::endTag( 'div' );
			} ?>
        </div>
    </div>
	<?php
}
?>