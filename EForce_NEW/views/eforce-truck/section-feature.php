<?php
/**
 * Created by PhpStorm.
 * User: ante
 * Date: 7/11/17
 * Time: 10:40 PM
 */

use rnd\helpers\Html;

/* @var $this \App\controllers\EforceTruckController*/
$featureText = get_field('feature_text', $this->pageID);
$featureImage = get_field('feature_image',$this->pageID);
?>
<div class="full-width">
    <div class="visionWrap">
        <div class="vision-right" style="background-image: url(<?= $featureImage['url'] ?>);"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <?= Html::tag('h4', $featureText) ?>
                </div>
            </div>
        </div>
    </div>
</div>


