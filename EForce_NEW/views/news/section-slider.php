<?php
/**
 * Author: Ante Galić
 */

use rnd\helpers\Html;

/* @var $this \App\controllers\NewsController */

$imageNews             = get_field( 'news_background', $this->pageID );
$newsTitle             = get_field( 'news_title', $this->pageID );
?>


<div class="full-width static eforce-container-with-bg general-padding text-white text-center news-page-header"
     style="background-image: url(<?= $imageNews['url'] ?>)">
    <div class="container">
        <div class="title-inner">
            <?= Html::tag( 'h2', $newsTitle, [ 'class' => 'fbold' ] ) ?>
        </div>
    </div>
</div>
