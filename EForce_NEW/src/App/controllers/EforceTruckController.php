<?php
/**
 * Created by PhpStorm.
 * User: ante
 * Date: 7/10/17
 * Time: 8:22 PM
 */

namespace App\controllers;

use rnd\web\Controller;


class EforceTruckController extends Controller
{
    protected $sections = [
      'slider',
      'feature',
      'lists',
      'practicality',
      'content-practicality',
      'economic',
      'enviromental',
      'technology',
      'specification',
    ];
}