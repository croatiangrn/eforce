<?php
/**
 * @author: Marko Mikulic
 */

namespace App\widgets;

use rnd\helpers\Html;
use Detection\MobileDetect;
use rnd\base\Component;

class Menu extends Component
{
	public static $counter = 1;
	protected $isMobile = false;

	public function init()
	{
		$deviceDetect   = new MobileDetect();
		$this->isMobile = $deviceDetect->isMobile();
	}

	protected function addPageActiveClass( $item, &$array = [], $itemTitle = '' )
	{
		if ($item->title == $itemTitle && in_array('current_page_parent', $item->classes))
		{
			array_push( $array, 'current_page_item');
		}
	}

	public function render( $location )
	{
		$items        = $this->getMenuItems( $location );

		echo Html::beginTag( 'ul', [ 'class' => 'nav navbar-nav' ] );
		foreach ( $items as $item ) {
			$has_children = ( key_exists( 'children', $item ) ) ? true : false;
			$title        = $item->title;
			$url          = $item->url;
			if ( $has_children ) {
				if ( $this->isMobile ) {
					$url = Html::a( $title, $url, [
						'data-toggle'   => 'dropdown',
						'role'          => 'button',
						'aria-haspopup' => 'true',
						'aria-expanded' => 'false',
					] );
				} else {
					$url = Html::a( $title, $url, [
						'data-hover'   => 'dropdown',
//						'data-target'   => '#',
						'role'          => 'button',
						'aria-haspopup' => 'true',
						'aria-expanded' => 'false',
					] );
				}

			} else {
				$url = Html::a( $title, $url );
			}

			$itemClasses = join( ' ', $item->classes );
			$itemClass   = $itemClasses;

			if ( $has_children ) {
				echo Html::beginTag( 'li', [ 'class' => 'dropdown ' . $itemClass ] );
				echo $url;
				if (static::$counter > 1) {
					$this->renderSubItems( $item->children, false, true );
				} else {
					$this->renderSubItems( $item->children );
				}
				static::$counter++;
				echo Html::endTag( 'li' );
			} else {
				$title = $item->title;
				$url   = $item->url;
				$url   = Html::a( $title, $url );
				echo Html::beginTag( 'li', [ 'class' => $itemClass ] );
				echo $url;
				echo Html::endTag( 'li' );
			}
		}
		echo Html::endTag( 'ul' );
	}

	/**
	 *
	 * @param $location
	 *
	 * @return array|null
	 */
	public function getMenuItems( $location )
	{
		$menu_id = $this->findMenuId( $location );
		$items   = wp_get_nav_menu_items( $menu_id );

		return $items ? $this->buildTree( $items, 0 ) : null;
	}

	/**
	 * Finds menu id by menu location
	 *
	 * @param string $location
	 *
	 * @return mixed
	 */
	private function findMenuId( $location )
	{
		$locations = get_nav_menu_locations();
		$menu_id   = $locations[ $location ];

		return $menu_id;
	}

	/**
	 * @param array $elements
	 * @param int   $parentId
	 *
	 * @return array
	 */
	/*protected function buildTree( array &$elements, $parentId = 0 )
	{
		$branch = [];

		foreach ( $elements as &$element ) {
			if ( $element->menu_item_parent == $parentId ) {
				$children = $this->buildTree( $elements, $element->ID );
				if ( $children ) {
					$element->children = $children;
				}

				$branch[ $element->ID ] = $element;
				unset( $element );
			}
		}

		return $branch;
	}*/
	protected function buildTree(array &$elements, $parentId = 0) {

		$branch = array();

		foreach ($elements as &$element) {

			if ($element->menu_item_parent == $parentId) {
				$children = $this->buildTree($elements, $element->ID);
				if ($children) {
					$element->children = $children;
				}
				$branch[$element->ID] = $element;
				unset($element);
			}
		}
		return $branch;
	}

	protected function renderSubItems( $items, $firstTime = true, $blog = false )
	{
		if ( $firstTime && !$blog) {
			echo Html::beginTag( 'ul', [ 'class' => 'sub-menu dropdown-menu top-nav-products really-products' ] );
		} else if ($blog){
			echo Html::beginTag( 'ul', [ 'class' => 'sub-menu dropdown-menu blog' ] );
		} else {
			$class = ($this->isMobile) ? 'sub-menu hidden' : 'sub-menu';
			echo Html::beginTag( 'ul', [ 'class' => $class ] );
		}

		foreach ( $items as $item ) {
			$itemClasses = join( ' ', $item->classes );
			$itemClass   = $itemClasses;
			echo Html::beginTag( 'li', [ 'class' => $itemClass ] );
			$title = $item->title;
			$url   = $item->url;
			$url   = Html::a( $title, $url );
			echo $url;
			$has_children = key_exists( 'children', $item );
			if ( $has_children ) {
				$this->renderSubItems( $item->children, false );
			}
			echo Html::endTag( 'li' );
		}

		echo Html::endTag( 'ul' );
	}
}