jQuery.noConflict();
jQuery(document).ready(function($){


    // Make #anchors scroll available
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    var testimonialsSlider = new Swiper('.testimonials-slider', {
        // pagination: '.swiper-pagination',
        // paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        loop: true,
        autoplay: 4500
    });

    var featuredPostsHomeSlider = new Swiper('.featuredPostsHomeSlider', {
        // pagination: '.swiper-pagination',
        // paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        loop: false,
        // autoplay: 4500
    });

    $('.video-iframe').magnificPopup({
        type: 'iframe',
        iframe: {
            patterns: {
                youtube: {
                    index: 'youtube.com/',
                    id: function(url) {
                        var m = url.match(/[\\?\\&]v=([^\\?\\&]+)/);
                        if ( !m || !m[1] ) return null;
                        return m[1];
                    },
                    src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0&showinfo=0'
                },
            }
        }
    });

    $('a.dropdown-toggle').on('click', function(e){
        var addressValue = $(this).attr("href");
    });
});