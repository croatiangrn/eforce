<?php
/**
 * @author: Marko Mikulic
 */

namespace App\controllers;


use rnd\web\Controller;

class AboutUsController extends Controller
{
	protected $sections = [
		'top',
		'careers',
		'contact',
		'map'
	];
}