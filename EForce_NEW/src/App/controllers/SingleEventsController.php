<?php
/**
 * Author: Ante Galić
 */

namespace App\controllers;


use rnd\web\Controller;

class SingleEventsController extends Controller
{
    protected $viewName = 'single-events';
    protected $sections = [
        'events-post',
    ];
}