<?php
/**
 * Author: Ante Galić
 */

use rnd\helpers\Html;

$title = get_field( 'content_title' );

?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="heading-download">
                    <?= Html::tag('h2', $title, ['class' => 'heading h2']) ?>
                </div>
            </div>
        </div>
    </div>
<?php
$args = [

	'post_type' => 'library',
];
query_posts( $args );

while ( have_posts() ) {
	the_post();
	$downloadTitle = get_field( 'content_title' );
	$downloadImage = get_field( 'content_image' )['url'];
	$file          = get_field( 'content_file' );
	$category      = get_the_category();
	$categoryName  = $category[0]->name;

	?>
    <a href="<?= $file['url']; ?>" target="_blank">Download</a>
    <div class="downloadImage"><?= Html::img( $downloadImage ) ?></div>


	<?php
}
wp_reset_query();
?>