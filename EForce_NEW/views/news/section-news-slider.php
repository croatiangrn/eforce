<?php
/**
 * Author: Ante Galić
 */

use rnd\helpers\Html;

/* @var $this \App\controllers\NewsController */

$events_icon        = get_field( 'events_header_icon', $this->pageID );
$events_title       = get_field( 'events_title', $this->pageID );
?>
<div class="full-width eforce-green-container general-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div><?= Html::img( $events_icon['url'] ) ?></div>
	            <?php
	            echo Html::tag( 'h2', $events_title, [ 'class' => 'heading h2' ] );
	            ?>
            </div>
        </div>
    </div>
</div>
