<?php
/**
 * Author: Ante Galić
 */

use rnd\helpers\Html;

/* @var $this \App\controllers\ImpressumController */
?>

<div class="full-width general-padding">
    <div class="container">
		<?php
		while ( have_rows( 'section_content' ) ) {
			the_row();
			$title = get_sub_field( 'title' );
			$text  = get_sub_field( 'text' );
			?>
            <div class="row">
                <div class="col-sm-12">
            <div><h2 class="impressum-heading h2"><?= $title; ?></h2></div>
            <div class="impressumText"><?= $text; ?></div>
                </div>
            </div>
			<?php
		}
		?>

    </div>
</div>
