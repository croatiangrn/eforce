<?php
/**
 * @author: Marko Mikulic
 */
use rnd\helpers\Html;

/* @var $this \App\controllers\HomeController */

$testimonials_title = get_field( 'testimonials_title');

if (have_rows( 'testimonials_list')) {
	?>
    <div class="full-width eforce-green-container general-padding">
        <div class="container">
            <?php
            echo Html::tag('h2', $testimonials_title, ['class' => 'heading h2']);
            ?>
            <div class="row">
                <div class="col-md-12">
                    <!-- Swiper -->
                    <div class="swiper-container testimonials-slider">
                        <div class="swiper-wrapper">
	                        <?php
                            while (have_rows( 'testimonials_list')) {
                                the_row();

	                            $logo         = get_sub_field( 'logo' )['url'];
	                            $company_name = get_sub_field( 'company_name' );
	                            $author       = get_sub_field( 'author' );
	                            $testimonial  = get_sub_field( 'testimonial' );
                                ?>
                                <div class="swiper-slide">
                                    <?php
                                    echo Html::img($logo);
                                    echo Html::tag('p', $testimonial);
                                    echo Html::tag('h3', $company_name);
                                    echo Html::tag('h2', $author);
                                    ?>
                                </div>
                                <?php
                            }
	                        ?>
                        </div>
                        <!-- Add Arrows -->
                        <div class="swiper-button-next swiper-button-white"></div>
                        <div class="swiper-button-prev swiper-button-white"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?php
}
?>