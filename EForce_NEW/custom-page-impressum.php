<?php
/**
 * Template name: Impressum
 * @author: Marko Mikulic
 */

use App\controllers\ImpressumController;

$controller = new ImpressumController();
$controller->render();