<?php
/**
 * @author: Marko Mikulic
 */
// Include composer files
require('vendor/autoload.php');
require ('vendor/croatiangrn/wp-yii/src/rnd/Rnd.php');

$config = [
	'id' => 'My App',
	'name' => get_bloginfo('name'),
	'language' => 'hr-HR'
];
$config = \rnd\helpers\ArrayHelper::merge(
	require(__DIR__ . '/vendor/croatiangrn/wp-yii/src/rnd/config.php'),
	$config
);

(new rnd\web\Application($config));

// Register JS & CSS to project theme
function theme_script_enqueue()
{
	$assets = new \App\assets\AppAsset();
	$assets->publish();
}
add_action('wp_enqueue_scripts', 'theme_script_enqueue');

// Enable Menu Locations
function enable_menus()
{
	add_theme_support( 'menus' );
	register_nav_menu( 'topmenu', 'Top Navigation Menu' );
}
add_action( 'init', 'enable_menus' );

// ACF options page
if( function_exists('acf_add_options_page') ) {
	acf_add_options_sub_page([
		'page_title' => 'Options',
		'menu_title' => 'Options',
	]);

	acf_add_options_sub_page([
		'page_title' => 'Events translations',
		'menu_title' => 'Events translations'
	]);

	acf_add_options_sub_page([
		'page_title' => 'Footer',
		'menu_title' => 'Footer'
	]);
}
add_theme_support( 'post-thumbnails' );

/**
 * Creates custom post types
 */
function create_custom_post_types() {
	register_post_type( 'news', [
		'labels'      => [
			'name'          => __( 'News' ),
			'singular_name' => __( 'News' )
		],
		'public'      => true,
		'has_archive' => true,
		'menu_icon'   => 'dashicons-welcome-write-blog',
		'rewrite'     => [ 'slug' => 'news' ],
		'supports' => ['title', 'editor', 'author', 'thumbnail']
	] );

	register_taxonomy( 'news-categories', 'news', [
		// Hierarchical taxonomy (like categories)
		'label' => 'News Categories',
		'hierarchical' => true,
		// This array of options controls the labels displayed in the WordPress Admin UI

		// Control the slugs used for this taxonomy
		'rewrite'      => [
			'slug'         => 'news-category',
			'with_front'   => false,
			'hierarchical' => true
		],
	] );

	register_post_type( 'library', [
		'labels'      => [
			'name'          => __( 'Library' ),
			'singular_name' => __( 'Library' )
		],
		'public'      => true,
		'has_archive' => true,
		'menu_icon'   => 'dashicons-media-archive',
		'rewrite'     => [ 'slug' => 'library' ],
	] );

	// Add new "Category" taxonomy to Posts
	register_taxonomy( 'library-categories', 'library', [
		// Hierarchical taxonomy (like categories)
		'label' => 'Library Categories',
		'hierarchical' => true,
		// This array of options controls the labels displayed in the WordPress Admin UI

		// Control the slugs used for this taxonomy
		'rewrite'      => [
			'slug'         => 'library-category',
			'with_front'   => false,
			'hierarchical' => true
		],
	] );

	register_post_type( 'events', [
		'labels'      => [
			'name'          => __( 'Events' ),
			'singular_name' => __( 'Event' )
		],
		'public'      => true,
		'has_archive' => true,
		'menu_icon'   => 'dashicons-calendar-alt',
		'rewrite'     => [ 'slug' => 'events' ],
		'supports' => ['title', 'editor', 'author', 'thumbnail']
	] );

	// Add new "Category" taxonomy to Posts
	register_taxonomy( 'events-categories', 'events', [
		// Hierarchical taxonomy (like categories)
		'label' => 'Events Categories',
		'hierarchical' => true,
		// This array of options controls the labels displayed in the WordPress Admin UI

		// Control the slugs used for this taxonomy
		'rewrite'      => [
			'slug'         => 'events-category',
			'with_front'   => false,
			'hierarchical' => true
		],
	] );

}
// Add a hook to create custom post types
add_action('init', 'create_custom_post_types');


add_filter( 'wp_get_nav_menu_items', 'prefix_nav_menu_classes', 10, 3 );
function prefix_nav_menu_classes( $items, $menu, $args )
{
	_wp_menu_item_classes_by_context( $items );

	return $items;
}