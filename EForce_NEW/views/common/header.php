<?php
/**
 * @author: Marko Mikulic
 */

use App\widgets\Menu;
use rnd\helpers\Html;

/* @var $this \rnd\web\Controller */

$logo = get_field('logo', 'options');
?>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title><?= $this->getPageTitle() ?></title>
	<?php wp_head() ?>
</head>
<body>
<header>
	<div class="full-width green-bg">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mojnavbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="site-logo">
	                        <?php
                            $logo_img = Html::img($logo['url'], ['class' => 'img-responsive']);

                            echo Html::a($logo_img, get_home_url(), ['class' => '']);
                            ?>
                        </div>
                    </div>
                    <?php
                    /*$menu = new Menu();
                    $menu->render( 'topmenu');*/

                    echo $this->renderMenu([
                        'theme_location' => 'topmenu',
                        'menu_class' => 'nav navbar-nav navbar-right',
                        'container_id' => 'mojnavbar'
                    ]);
                    ?>
                </div>
                <!--/.container-fluid -->
            </nav>
        </div>
	</div>
</header>
<div id="wrapper">