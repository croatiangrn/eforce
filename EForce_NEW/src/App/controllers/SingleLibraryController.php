<?php
/**
 * Author: Ante Galić
 */

namespace App\controllers;


use rnd\web\Controller;

class SingleLibraryController extends Controller

{
    protected $viewName = 'single-library';
    protected $sections = [
            'downloads-post',
    ];
}