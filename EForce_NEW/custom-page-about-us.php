<?php
/**
 * Template name: About Us
 * @author: Marko Mikulic
 */

use App\controllers\AboutUsController;

$controller = new AboutUsController();
$controller->render();