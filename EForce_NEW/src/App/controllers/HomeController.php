<?php
/**
 * @author: Marko Mikulic
 */

namespace App\controllers;


use rnd\web\Controller;

class HomeController extends Controller
{
	protected $sections = [
		'slider',
		'features',
		'video',
		'testimonials',
		'latest-post'
	];
}