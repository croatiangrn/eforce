<?php
/**
 * Created by PhpStorm.
 * User: ante
 * Date: 7/25/17
 * Time: 9:18 PM
 */

use rnd\helpers\Html;

/* @var $this \App\controllers\EforceTruckController */
$image        = get_field( 'section_feature_image', $this->pageID )['url'];
$title        = get_field( 'section_feature_title', $this->pageID );
$text         = get_field( 'section_feture_text', $this->pageID );
?>
<div class="full-width eforce-green-container general-padding" id="<?= $title ?>">
    <div class="optioncontent">
        <div class="container">
	        <?php
	        if ($image !== null) {
		        ?>
                <div><?= Html::img($image) ?></div>
		        <?php
	        }
	        ?>
            <?= Html::tag('h2', $title, ['class' => 'heading h2']) ?>
            <div class="row">
                <div class="col-md-12">
					<?= $text; ?>
                </div>
            </div>
        </div>
    </div>
</div>
