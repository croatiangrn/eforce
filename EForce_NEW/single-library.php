<?php
/**
 * Template name: Single-Library
 * Author: Ante Galić
 */

use App\controllers\SingleLibraryController;

$controller = new SingleLibraryController();
$controller->render();