<?php
/**
 * Author: Ante Galić
 */

namespace App\controllers;


use rnd\web\Controller;
use WP_Query;

class NewsController extends Controller
{
	protected $sections = [
		'slider',
		'latest-news',
		'news-slider',
		'events',
	];

	protected function setParamsLatestNews()
	{
		$posts_per_page = 4;
		$args           = [
			'post_type'      => 'news',
			'posts_per_page' => $posts_per_page
		];

		$latest_news = new WP_Query( $args );

		return [
			'latest_news'    => $latest_news,
			'posts_per_page' => $posts_per_page
		];
	}

	protected function setParamsEvents()
	{
		$posts_per_page = 4;
		$args           = [
			'post_type'      => 'events',
			'posts_per_page' => $posts_per_page
		];

		$latest_events = new WP_Query( $args );

		return [
			'posts_per_page' => $posts_per_page,
			'latest_events'  => $latest_events
		];
    }
}