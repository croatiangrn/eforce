<?php
/**
 * Template name: Elementor Template
 *
 * @author: Marko Mikulic
 */

use App\controllers\ElementorController;

$controller = new ElementorController();
$controller->render();