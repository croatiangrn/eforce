<?php
/**
 * Template name: Home
 * @author: Marko Mikulic
 */

use App\controllers\HomeController;
$controller = new HomeController();
$controller->render();