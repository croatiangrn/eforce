<?php
/**
 * Author: Ante Galić
 */
use rnd\helpers\Html;

/* @var $this \App\controllers\EforceTruckController */
$image = get_field('evniromental_image', $this->pageID)['url'];
$title = get_field('enviromental_title', $this->pageID);
$text = get_field('evniromental_text', $this->pageID);
?>

<div class="full-width eforce-green-container general-padding" id="<?=$title;?>">
    <div class="optioncontent">
        <div class="container">
	        <?php
	        if ($image !== null) {
		        ?>
                <div><?= Html::img($image) ?></div>
		        <?php
	        }
	        ?>
            <h2 class="heading h2"><?= $title; ?></h2>
            <div class="row">
                <div class="col-md-12">
                    <?= $text; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
if (have_rows('evniromental_content')) {
	?>
    <div class="optionpracticality">
        <div class="container">
			<?php
			while ( have_rows( 'evniromental_content' ) ) {
				the_row();
				$economicText  = get_sub_field( 'evniromental_title' );
				$economicImage = get_sub_field( 'evniromental_image' )['url'];
				echo Html::beginTag( 'div', [ 'class' => 'row practicalitypart' ] );
				echo Html::beginTag( 'div', [ 'class' => 'col-sm-12' ] );
				echo Html::tag( 'h4', $economicText, [ 'class' => 'small-heading' ] );
				echo Html::endTag( 'div' );
				echo Html::beginTag( 'div', [ 'class' => 'col-sm-6 col-sm-offset-4' ] );
				echo Html::img( $economicImage, [ 'class' => 'img-responsive' ] );
				echo Html::endTag( 'div' );
				echo Html::endTag( 'div' );
			}
			?>
        </div>
    </div>
	<?php
}
?>