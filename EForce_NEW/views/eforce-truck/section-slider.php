<?php
/**
 * Created by PhpStorm.
 * User: ante
 * Date: 7/10/17
 * Time: 8:48 PM
 */
use rnd\helpers\Html;

/* @var $this \App\controllers\EforceTruckController*/

$slider = get_field('slider_shortcode', $this->pageID );

echo Html::tag('div', do_shortcode($slider), ['class' => 'homepage-slider']);
?>
