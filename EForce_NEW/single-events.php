<?php
/**
 * Template name: Single-Events
 *
 * Author: Ante Galić
 */

use App\controllers\SingleEventsController;

$controller = new SingleEventsController();
$controller->render();