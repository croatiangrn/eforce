<?php
/**
 * Created by PhpStorm.
 * User: ante
 * Date: 7/24/17
 * Time: 10:27 PM
 */
use rnd\helpers\Html;

/* @var $this \App\controllers\EforceTruckController*/
if (have_rows('features_section_')) {
	?>
    <div class="full-width">
        <div class="selectOptionWrap">
            <div class="container">
                <ul class="featuredTabs">
					<?php

					while ( have_rows( 'features_section_' ) ) {
						the_row();
						$title = get_sub_field( 'title' );
						$image = get_sub_field( 'image' )['url'];
						?>
                        <li>
                            <a href="#<?= $title; ?>">
                                <img src="<?= $image; ?>">
                                <span><?= $title; ?></span>
                            </a>
                        </li>
						<?php
					}
					?>
                </ul>
            </div>
        </div>
    </div>
	<?php
}
?>

