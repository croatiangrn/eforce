<?php
/**
 * @author: Marko Mikulic
 */

use rnd\helpers\Html;

$footerImage       = get_field( 'content_footer_image', 'options' );
$subscribeText     = get_field( 'content_footer_subscribe_text', 'options' );
$copyrightText     = get_field( 'copyright_text', 'options' );
$form              = get_field( 'subscribe_form', 'options' );
$impressumPageLInk = get_field( 'impressum_page_link', 'options' );
$agbPageLink       = get_field( 'agb_page_link', 'options' );

?>
</div><!-- #wrapper -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
				<?= Html::img( $footerImage['url'], [ 'alt' => $footerImage['alt'], 'class' => 'img-responsive' ] ) ?>
            </div>
            <div class="col-sm-6">
                <div class="subscribeWrap">
					<?php
					echo Html::tag( 'p', $subscribeText );
					echo Html::tag( 'div', do_shortcode( $form ), [ 'class' => 'footer-newsletter-form' ] ); ?>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <div class="impressumLink">
					<?= Html::a( 'AGB', get_permalink($agbPageLink->ID), [ 'class' => 'impressumLink' ] ); ?>
                    <span class="impressumLink text-white"> | </span>
					<?= Html::a( 'Impressum', get_permalink($impressumPageLInk->ID), [ 'class' => 'impressumLink' ] ); ?>
                </div>
            </div>
            <div class="col-sm-7">
		        <?= Html::tag('p', date( 'Y' ) . ' ' . $copyrightText, ['class' => 'copyright']) ?>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>
</html>
