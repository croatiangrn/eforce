<?php
/**
 * Author: Marko Mikulic
 */

use rnd\helpers\Html;

/* @var $this \App\controllers\DownloadsController */
/* @var $downloads_query WP_Query */
?>
<div class="full-width static general-padding about-us-container text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
				<?php
				echo Html::tag( 'h2', $this->getPageTitle( false ), [ 'class' => 'heading h2' ] );
				?>
            </div>
        </div>
        <div class="row">
			<?php

			$downloads_categories = get_terms( [
				'taxonomy' => 'library-categories'
			] );

			foreach ( $downloads_categories as $category ) {


				$query = new WP_Query( [
					'post_type' => 'library',
					'tax_query' => [
						[
							'taxonomy' => 'library-categories',
							'field'    => 'slug',
							'terms'    => $category->slug
						]
					]
				] );
				if ( $query->have_posts() ) {
				    ?>
                    <div class="col-md-12 downloads-category-title">
                        <h3 class="text-uppercase text-left"><?= $category->name ?></h3>
                    </div>
                    <?php
					while ( $query->have_posts() ) {
						$query->the_post();
						$file2download  = get_field( 'file_2_download' ); // Returns URL only! No need for ['url']
						$post_thumbnail = get_field( 'thumbnail' ); // Returns URL only! No need for ['url']

						// TODO: Move 'download' string to ACF Options page
						?>
                        <div class="col-md-3 col-lg-3">
                            <div class="downloads-box">
                                <div class="media">
                                    <div class="media-body">
										<?= Html::a( get_the_title(), $file2download, [
											'target' => '_blank',
											'class'  => 'media-title'
										] ); ?>
                                        <div class="media-icon">
											<?= Html::img( $post_thumbnail, [ 'class' => 'img-responsive mt-75' ] ) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<?php
					}
				}
			}

			wp_reset_query();
			?>
        </div>

    </div>
</div>